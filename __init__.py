"""
  __init__.py
  
  State Estimation and Analysis for PYthon

    Module for working with oceanographic data and models

  Written by Brian Powell on 10/18/13
  Copyright (c)2013 University of Hawaii under the BSD-License.
  
  Requires the following packages: joblib
  
  Import classes include:
  
  - :class:`~seapy.environ.opt`
  - :class:`~seapy.progressbar.ProgressBar`
  
  Imported functions include:

  - :func:`~seapy.lib.adddim`
  - :func:`~seapy.lib.convolve_mask`
  - :func:`~seapy.lib.earth_distance`
  - :func:`~seapy.lib.rotate`
  - :func:`~seapy.lib.vecfind`
  - :func:`~seapy.lib.list_files`
  - :func:`~seapy.lib.day2date`
  - :func:`~seapy.lib.date2day`
  - :func:`~seapy.lib.today2day`
  - :func:`~seapy.oa.oasurf`
  - :func:`~seapy.oa.oavol`
  - :func:`~seapy.progressbar.progress`
  
"""

from .oa import *
from .lib import *
from .environ import opt
from . import roms
from . import model
from . import qserver
from .map import map
from .hawaii import hawaii
from . import plot
from .progressbar import *