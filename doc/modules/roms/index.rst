======================
:mod:`seapy.roms`
======================


.. automodule:: seapy.roms.__init__
   :members:
   :show-inheritance:
   
.. automodule:: seapy.roms.lib
   :members:
   :show-inheritance:



:mod:`boundary`
----------------------------------
.. automodule:: seapy.roms.boundary
   :members:
   :show-inheritance:


:mod:`initial`
----------------------------------
.. automodule:: seapy.roms.initial
   :members:
   :show-inheritance:


:mod:`interp`
----------------------------------
.. automodule:: seapy.roms.interp
   :members:
   :show-inheritance:


:mod:`ncgen`
----------------------------------
.. automodule:: seapy.roms.ncgen
   :members:
   :show-inheritance:


:mod:`obs`
----------------------------------
.. automodule:: seapy.roms.obs
   :members:
   :show-inheritance:




