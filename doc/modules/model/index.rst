======================
:mod:`seapy.model`
======================


.. automodule:: seapy.model.__init__
   :members:
   :show-inheritance:
   
.. automodule:: seapy.model.lib
   :members:
   :show-inheritance:



:mod:`grid`
----------------------------------
.. automodule:: seapy.model.grid
  :noindex:
  
.. autoclass:: seapy.model.grid
   :members:
   :show-inheritance:


:mod:`soda`
----------------------------------
.. automodule:: seapy.model.soda
   :members:
   :show-inheritance:

